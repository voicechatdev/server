package tk.andr3as07.voice.server;

import java.util.ArrayList;
import java.util.List;

public class ChannelHandler {

	private final List<Channel> channels;

	public ChannelHandler() {
		this.channels = new ArrayList<Channel>();
	}

	public boolean addChannel(final Channel channel) {
		if (this.existsChannel(channel))
			return false;
		this.channels.add(channel);
		return true;
	}

	public boolean existsChannel(final Channel channel) {
		for (final Channel item : this.channels)
			if (item.getPersistentID() == channel.getPersistentID())
				return true;
		return false;
	}

	public Channel getChannel(final long persistent) {
		for (final Channel item : this.channels)
			if (item.getPersistentID() == persistent)
				return item;
		return null;
	}

	public boolean removeChannel(final Channel channel) {
		if (!this.existsChannel(channel))
			return false;
		return this.channels.remove(channel);
	}
}