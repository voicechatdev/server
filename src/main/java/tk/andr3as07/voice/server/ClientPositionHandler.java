package tk.andr3as07.voice.server;

import java.util.*;

import tk.andr3as07.util.log.Logger;

public class ClientPositionHandler {

	private final Map<Integer, Long> positions; // Client Channel
	private long defaultChannel;

	public ClientPositionHandler() {
		this.positions = new HashMap<Integer, Long>();
	}

	public Channel getChannel(final Client client) {
		return this.getChannel(client.getSessionID());
	}

	public Channel getChannel(final int session) {
		final long id = this.getChannelID(session);
		if (id == 0)
			return null;
		return Server.getInstance().getChannelHandler().getChannel(id);
	}

	public long getChannelID(final Client client) {
		return this.getChannelID(client.getSessionID());
	}

	public long getChannelID(final int session) {
		if (!this.positions.containsKey(session))
			return 0;
		return this.positions.get(session);
	}

	public List<Integer> getClientIDs(final Channel channel) {
		return this.getClientIDs(channel.getPersistentID());
	}

	public List<Integer> getClientIDs(final long channel) {
		final List<Integer> ids = new ArrayList<Integer>();
		for (final int client : this.positions.keySet())
			if (this.positions.get(client) == channel)
				ids.add(client);
		return ids;
	}

	public List<Client> getClients(final Channel channel) {
		return this.getClients(channel.getPersistentID());
	}

	public List<Client> getClients(final long channel) {
		final List<Client> clients = new ArrayList<Client>();
		for (final int id : this.getClientIDs(channel))
			clients.add(Server.getInstance().getClientHandler().getClient(id));
		return clients;
	}

	public boolean moveClient(final Client client, final Channel channel) {
		if (this.getChannelID(client.getSessionID()) == channel.getPersistentID())
			return false;
		this.positions.put(client.getSessionID(), channel.getPersistentID());
		Logger.info("\"" + client.getDisplayName() + "\" moved to Channel \"" + channel.getName() + "\"");
		return true;
	}

	public boolean moveClient(final int session, final long channel) {
		if (this.getChannelID(session) == channel)
			return false;
		this.positions.put(session, channel);
		return true;
	}

	public boolean moveToDefault(final Client client) {
		return this.moveClient(client, Server.getInstance().getChannelHandler().getChannel(this.defaultChannel));
	}

	public boolean moveToDefault(final int session) {
		return this.moveClient(session, this.defaultChannel);
	}

	public List<Integer> removeChannel(final Channel channel) {
		return this.removeChannel(channel.getPersistentID());
	}

	public List<Integer> removeChannel(final long channel) {
		final List<Integer> ids = this.getClientIDs(channel);

		// Move Clients
		for (final int id : ids)
			this.moveToDefault(id);
		return ids;
	}

	public long removeClient(final Client client) {
		return this.removeClient(client.getSessionID());
	}

	public long removeClient(final int session) {
		return this.positions.remove(session);
	}

	public boolean setDefaultChannel(final Channel channel) {
		if (channel.getPersistentID() == this.defaultChannel)
			return false;
		if (Server.getInstance().getChannelHandler().existsChannel(channel)) {
			this.defaultChannel = channel.getPersistentID();
			return true;
		}
		return false;
	}
}