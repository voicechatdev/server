package tk.andr3as07.voice.server;

import static tk.andr3as07.voice.server.JoinResponse.*;

import tk.andr3as07.util.log.Logger;
import tk.andr3as07.voice.server.group.GroupHandler;
import tk.andr3as07.voice.server.leave.ILeaveReason;
import tk.andr3as07.voice.server.leave.StoppingLeaveReason;

public class Server {

	private static Server instance;

	public static final Server getInstance() {
		if (instance == null)
			instance = new Server();
		return instance;
	}

	private final ClientHandler clients;
	private final BanHandler bans;
	private final ChannelHandler channels;
	private final ClientPositionHandler positions;
	private final GroupHandler groups;
	private Status status = Status.UNKNOWN;
	private int slots = 0;
	private int reserved = 0;

	public Server() {
		this.clients = new ClientHandler();
		this.bans = new BanHandler();
		this.channels = new ChannelHandler();
		this.positions = new ClientPositionHandler();
		this.groups = new GroupHandler();
		this.status = Status.OFFLINE;
	}

	@SuppressWarnings("incomplete-switch")
	public JoinResponse canJoin(final Client client) {
		// Prepare Client
		this.prepareClient(client);

		// Already Connected
		if (this.clients.isConnected(client))
			return USER_ALREADY_CONNECTED;

		// Server Status
		switch (this.status) {
		case LOCKED:
			return SERVER_LOCKED;
		case STARTING:
			return SERVER_STARTING;
		case STOPPING:
			return SERVER_STOPPING;
		case UNKNOWN:
			return SERVER_STATUS_UNKNOWN;
		case OFFLINE:
			return SERVER_OFFLINE;
		}

		// Server is Full
		// TODO: Make permission based
		if (this.isFull())
			return SERVER_SLOT_LIMIT_REACHED;

		// User is Banned
		// TODO: Make permission based

		// User
		if (this.bans.isBanned(client.getPersistentID()))
			return USER_BANNED_PERMANENT;

		// IP
		if (this.bans.isBanned(client.getAddress()))
			return USER_BANNED_IP_PERMANENT;

		// Familiar Limit
		// TODO: Make permission based
		if (this.clients.getFamiliarCount(client.getPersistentID()) >= 3)
			return USER_FAMILIAR_LIMIT_REACHED;

		return GRANTED;
	}

	public BanHandler getBanHandler() {
		return this.bans;
	}

	public ChannelHandler getChannelHandler() {
		return this.channels;
	}

	public ClientHandler getClientHandler() {
		return this.clients;
	}

	public GroupHandler getGroupHandler() {
		return this.groups;
	}

	public ClientPositionHandler getPositionHandler() {
		return this.positions;
	}

	public int getReservedSlotCount() {
		return this.reserved;
	}

	public int getSlotCount(final boolean countReserved) {
		return countReserved ? this.slots : this.slots - this.reserved;
	}

	public boolean isFull() {
		return this.clients.getClientCount() >= this.getSlotCount(false);
	}

	public boolean isFullUseReserved() {
		return this.clients.getClientCount() >= this.getSlotCount(true);
	}

	public int join(final Client client) {
		final JoinResponse response = this.canJoin(client);
		if (response == GRANTED) {
			Logger.info("\"" + client.getDisplayName() + "\" joined");

			// Get Session
			final int session = this.clients.addClient(client);

			// Connect to Default Channel
			this.positions.moveToDefault(client);

			return session;
		}
		Logger.info("\"" + client.getDisplayName() + "\" tried to join [" + response + "]");
		return 0;
	}

	public void leave(final Client client, final ILeaveReason reason) {
		Logger.info("\"" + client.getDisplayName() + "\" left the Server [" + reason.getLogMessage() + "]");

		// TODO: Send client the Leave
		this.removeClientFromServer(client);
	}

	public boolean lock() {
		if (this.status == Status.ONLINE) {
			Logger.info("Server Locked");
			this.status = Status.LOCKED;
			return true;
		}
		return false;
	}

	// public void kick(final Client client) {
	//
	// // Do the kick Stuff
	//
	// Logger.info("Client " + client.getPersistentID() + " kicked");
	// this.clients.removeClient(client.getSessionID());
	// }

	public boolean move(final Client client, final Channel channel) {
		// TODO: Update old Channel
		// TODO: Update new Channel
		// TODO: Update User
		return this.positions.moveClient(client, channel);
	}

	public boolean open() {
		if (this.status == Status.LOCKED) {
			Logger.info("Server Opened");
			this.status = Status.ONLINE;
			return true;
		}
		return false;
	}

	private void prepareClient(final Client client) {
		// TODO: Load Client from DB
	}

	private void removeClientFromServer(final Client client) {
		// Remove from Channels
		final long channel = this.positions.removeClient(client.getSessionID());

		// TODO: Update Channel

		// TODO: Remove Temporary Group Assignment

		// Remove client from Server
		this.clients.removeClient(client.getSessionID());

		// Terminate Client
		this.terminateClient(client);
	}

	public boolean setReservedSlotCount(final int slots) {
		if (slots > this.slots)
			return false;
		this.reserved = slots;
		return true;
	}

	public void setSlotCount(final int slots) {
		this.slots = slots;
	}

	public void start() {
		this.status = Status.STARTING;
		Logger.info("Server Starting");

		// Startup Server

		Logger.info("Server Started");
		this.status = Status.ONLINE;
	}

	public void stop() {
		this.status = Status.STOPPING;
		Logger.info("Server Stopping");

		// Kick all Clients
		final ILeaveReason reason = new StoppingLeaveReason();
		for (final int session : this.clients.getAllClientsSessionID())
			this.leave(this.clients.getClient(session), reason);

		// Shutdown Server

		Logger.info("Server Stopped");
		this.status = Status.OFFLINE;
	}

	private void terminateClient(final Client client) {
		// TODO: Save Client to DB
	}
}