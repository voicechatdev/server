package tk.andr3as07.voice.server;

import java.net.InetAddress;
import java.util.List;

import tk.andr3as07.voice.server.group.ChannelGroup;
import tk.andr3as07.voice.server.group.ServerGroup;
import tk.andr3as07.voice.server.leave.KickLeaveReason;

public class Client {

	private static int next = 0;

	private static final int next() {
		return ++next;
	}

	private final long persistent;
	private final int temp;
	private final InetAddress address;
	// private String platform;

	public Client(final long persistent, final InetAddress address) {
		this.persistent = persistent;
		this.address = address;
		this.temp = next();
	}

	public void banClient() {
		Server.getInstance().getBanHandler().addBan(this.persistent);
	}

	public void banIP() {
		Server.getInstance().getBanHandler().addBan(this.address);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final Client other = (Client) obj;
		if (this.persistent != other.persistent)
			return false;
		if (this.temp != other.temp)
			return false;
		return true;
	}

	public InetAddress getAddress() {
		return this.address;
	}

	public Channel getChannel() {
		return Server.getInstance().getPositionHandler().getChannel(this.getSessionID());
	}

	public ChannelGroup getChannelgroup() {
		return this.getChannelgroup(this.getChannel());
	}

	public ChannelGroup getChannelgroup(final Channel channel) {
		return Server.getInstance().getGroupHandler().getClientChannelgroup(this, channel);
	}

	public String getDisplayName() {
		return "Client " + this.getPersistentID();
	}

	public long getPersistentID() {
		return this.persistent;
	}

	public List<ServerGroup> getServerGroups() {
		return Server.getInstance().getGroupHandler().getClientServergroups(this);
	}

	public int getSessionID() {
		return Server.getInstance().getClientHandler().getSessionID(this);
	}

	public int getTemporaryID() {
		return this.temp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this.persistent ^ this.persistent >>> 32);
		result = prime * result + this.temp;
		return result;
	}

	public void kick(final String message) {
		Server.getInstance().leave(this, new KickLeaveReason(message));
	}

	public boolean move(final Channel channel) {
		return Server.getInstance().move(this, channel);
	}

	@Override
	public String toString() {
		return "{\"persistent\":" + this.persistent + ",\"temp\":" + this.temp + ",\"session\":" + this.getSessionID()
				+ ",\"servergroups\":" + this.getServerGroups() + "}";
	}
}