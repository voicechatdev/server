package tk.andr3as07.voice.server;

public enum Status {

	OFFLINE, STARTING, ONLINE, LOCKED, STOPPING, UNKNOWN;

}