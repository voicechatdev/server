package tk.andr3as07.voice.server;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class BanHandler {

	// Temp Code
	private final List<Long> clientBans;
	private final List<String> ipBans;

	public BanHandler() {
		this.clientBans = new ArrayList<Long>();
		this.ipBans = new ArrayList<String>();
	}

	public void addBan(final InetAddress address) {
		if (!this.isBanned(address))
			this.ipBans.add(address.getHostAddress().toLowerCase());
	}

	public void addBan(final long persistent) {
		if (!this.isBanned(persistent))
			this.clientBans.add(persistent);
	}

	public boolean isBanned(final InetAddress address) {
		return this.ipBans.contains(address.getHostAddress().toLowerCase());
	}

	public boolean isBanned(final long persistent) {
		return this.clientBans.contains(persistent);
	}

	public void removeBan(final InetAddress address) {
		this.clientBans.remove(address.getHostAddress().toLowerCase());
	}

	public void removeBan(final long persistent) {
		this.clientBans.remove(this.clientBans);
	}
}