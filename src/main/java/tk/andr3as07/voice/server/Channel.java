package tk.andr3as07.voice.server;

public class Channel {

	private final long persistent;
	private final long parent;
	private final byte sort;
	private final String name;

	public Channel(final long persistent, final long parent, final byte sort, final String name) {
		this.persistent = persistent;
		this.parent = parent;
		this.sort = sort;
		this.name = name;
	}

	public Channel(final long persistent, final String name) {
		this(persistent, 0, (byte) 0, name);
	}

	public String getName() {
		return this.name;
	}

	public long getParentID() {
		return this.parent;
	}

	public long getPersistentID() {
		return this.persistent;
	}

	public byte getSortPriority() {
		return this.sort;
	}

	public boolean setAsDefault() {
		return Server.getInstance().getPositionHandler().setDefaultChannel(this);
	}

	@Override
	public String toString() {
		return "{\"persistent\":" + this.persistent + ",\"parent\":" + this.parent + ",\"sort\":" + this.sort
				+ ",\"name\":\"" + this.name + "\"}";
	}
}