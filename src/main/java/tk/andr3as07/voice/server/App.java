package tk.andr3as07.voice.server;

import java.net.InetAddress;
import java.net.UnknownHostException;

import tk.andr3as07.util.log.Logger;
import tk.andr3as07.voice.server.group.ChannelGroup;
import tk.andr3as07.voice.server.group.ServerGroup;
import tk.andr3as07.voice.server.leave.DisconnectLeaveReason;
import tk.andr3as07.voice.server.leave.TimeoutLeaveReason;

public class App {

	public static void main(final String[] args) throws UnknownHostException {
		// Server Setup
		Logger.setTimeFormat("YYYY-MM-dd hh:mm:ss");
		final Server server = Server.getInstance();

		server.setSlotCount(32);
		server.setReservedSlotCount(5);

		final Channel hangar = new Channel(1, "Hangar Deck"), communications = new Channel(2, "Comminications"),
				brig = new Channel(3, "Brig"), cac = new Channel(4, "Command and Controll"),
				sickbay = new Channel(5, "Sickbay");

		server.getChannelHandler().addChannel(hangar);
		server.getChannelHandler().addChannel(communications);
		server.getChannelHandler().addChannel(brig);
		server.getChannelHandler().addChannel(cac);
		server.getChannelHandler().addChannel(sickbay);

		hangar.setAsDefault();

		final ServerGroup sOwner = new ServerGroup("ServerOwner", 1), sAdmin = new ServerGroup("Server Admin", 2),
				sMod = new ServerGroup("Moderator", 3), sMember = new ServerGroup("Server member", 4),
				sGuest = new ServerGroup("Server Guest", 5);

		server.getGroupHandler().addServergroup(sOwner);
		server.getGroupHandler().addServergroup(sAdmin);
		server.getGroupHandler().addServergroup(sMod);
		server.getGroupHandler().addServergroup(sMember);
		server.getGroupHandler().addServergroup(sGuest);

		final ChannelGroup cAdmin = new ChannelGroup("Channel Admin", 1), cOp = new ChannelGroup("Channel Operator", 2),
				cMember = new ChannelGroup("Channel Member", 3), cGuest = new ChannelGroup("Channel Guest", 4);

		server.getGroupHandler().addChannelgroup(cAdmin);
		server.getGroupHandler().addChannelgroup(cOp);
		server.getGroupHandler().addChannelgroup(cMember);
		server.getGroupHandler().addChannelgroup(cGuest);

		server.getGroupHandler().setDefaultServergroup(sGuest);
		server.getGroupHandler().setDefaultChannelgroup(cGuest);

		final Client andr3as07 = new Client(17, InetAddress.getLocalHost()),
				theSebware = new Client(42, InetAddress.getByName("42.42.42.42")),
				someOtherGuy = new Client(18, InetAddress.getByName("DEFC::0001")),
				ipBannedUser = new Client(234234, InetAddress.getByName("34.200.54.13")),
				idBannedUser = new Client(25157, InetAddress.getByName("34.200.54.14"));

		server.getBanHandler().addBan(InetAddress.getByName("34.200.54.13"));
		server.getBanHandler().addBan(25157);

		// Start Test

		server.join(andr3as07);

		server.start();

		server.join(ipBannedUser);
		server.join(idBannedUser);

		server.join(theSebware);
		server.join(andr3as07);
		server.join(someOtherGuy);
		server.join(theSebware);

		Logger.debug(Server.getInstance().getPositionHandler().getClients(hangar));

		theSebware.move(communications);

		Logger.debug(Server.getInstance().getPositionHandler().getClients(hangar));

		server.leave(andr3as07, new TimeoutLeaveReason());
		server.join(andr3as07);

		server.leave(theSebware, new DisconnectLeaveReason());
		server.lock();
		server.join(theSebware);

		server.open();

		server.join(new Client(17, InetAddress.getByName("42.42.42.42")));
		server.join(new Client(17, InetAddress.getByName("42.42.42.42")));
		server.join(new Client(17, InetAddress.getByName("42.42.42.42")));

		// Servergroups
		server.getGroupHandler().addServergroupToClient(andr3as07, sOwner);

		server.getGroupHandler().addServergroupToClient(theSebware, sOwner);
		server.getGroupHandler().addServergroupToClient(theSebware, sAdmin);
		server.getGroupHandler().addServergroupToClient(theSebware, sMod);
		server.getGroupHandler().removeServergroupFromClient(theSebware, sOwner);

		// Channelgroups
		server.getGroupHandler().setChannelgroupOfClient(andr3as07, cac, cAdmin);
		server.getGroupHandler().setChannelgroupOfClient(andr3as07, communications, cMember);
		server.getGroupHandler().setChannelgroupOfClient(andr3as07, hangar, cMember);

		server.getGroupHandler().setChannelgroupOfClient(theSebware, communications, cOp);

		server.getGroupHandler().setChannelgroupOfClient(someOtherGuy, communications, cOp);

		Logger.debug("Andr3as07");
		Logger.debug(server.getGroupHandler().getClientServergroups(andr3as07));
		Logger.debug(andr3as07.getChannelgroup(hangar));
		Logger.debug(andr3as07.getChannelgroup(communications));
		Logger.debug(andr3as07.getChannelgroup(brig));
		Logger.debug(andr3as07.getChannelgroup(cac));
		Logger.debug(andr3as07.getChannelgroup(sickbay));

		Logger.debug("TheSebware");
		Logger.debug(server.getGroupHandler().getClientServergroups(theSebware));
		Logger.debug(theSebware.getChannelgroup(hangar));
		Logger.debug(theSebware.getChannelgroup(communications));
		Logger.debug(theSebware.getChannelgroup(brig));
		Logger.debug(theSebware.getChannelgroup(cac));
		Logger.debug(theSebware.getChannelgroup(sickbay));

		Logger.debug("SomeOtherGuy");
		Logger.debug(server.getGroupHandler().getClientServergroups(someOtherGuy));
		Logger.debug(someOtherGuy.getChannelgroup(hangar));
		Logger.debug(someOtherGuy.getChannelgroup(communications));
		Logger.debug(someOtherGuy.getChannelgroup(brig));
		Logger.debug(someOtherGuy.getChannelgroup(cac));
		Logger.debug(someOtherGuy.getChannelgroup(sickbay));

		server.stop();

		server.join(andr3as07);
	}
}