package tk.andr3as07.voice.server;

import java.util.*;

public class ClientHandler {

	private static int next = 0;

	private static final int next() {
		return ++next;
	}

	private final Map<Integer, Client> clients;

	public ClientHandler() {
		this.clients = new HashMap<Integer, Client>();
	}

	public int addClient(final Client client) {
		// TODO: Handle duplicates
		final int session = next();
		this.clients.put(session, client);
		return session;
	}

	public List<Client> familiar(final long persistent) {
		final List<Client> clients = new ArrayList<Client>();
		for (final int session : this.clients.keySet())
			if (this.clients.get(session).getPersistentID() == persistent)
				clients.add(this.clients.get(session));
		return clients;
	}

	public List<Integer> getAllClientsSessionID() {
		final List<Integer> ids = new ArrayList<Integer>();
		for (final int id : this.clients.keySet())
			ids.add(id);
		return ids;
	}

	public Client getClient(final int session) {
		return this.clients.get(session);
	}

	public int getClientCount() {
		return this.clients.size();
	}

	public int getFamiliarCount(final long persistent) {
		return this.familiar(persistent).size();
	}

	public int getSessionID(final Client client) {
		for (final int session : this.clients.keySet())
			if (this.clients.get(session).getTemporaryID() == client.getTemporaryID())
				return session;
		return 0;
	}

	public boolean isConnected(final Client client) {
		for (final int session : this.clients.keySet())
			if (this.clients.get(session).getTemporaryID() == client.getTemporaryID())
				return true;
		return false;
	}

	public boolean isConnected(final int session) {
		return this.getClient(session) != null;
	}

	public boolean removeClient(final int session) {
		if (!this.clients.containsKey(session))
			return false;
		this.clients.remove(session);
		return true;
	}
}