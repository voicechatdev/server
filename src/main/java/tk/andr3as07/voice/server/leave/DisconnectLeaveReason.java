package tk.andr3as07.voice.server.leave;

import tk.andr3as07.voice.server.Side;

public class DisconnectLeaveReason implements ILeaveReason {

	public DisconnectLeaveReason() {
	}

	@Override
	public String getClientMessage() {
		return "You disconneted from the Server";
	}

	@Override
	public String getLogMessage() {
		return "Disconncted";
	}

	@Override
	public Side sideIssued() {
		return Side.CLIENT;
	}
}