package tk.andr3as07.voice.server.leave;

import tk.andr3as07.voice.server.Side;

public class KickLeaveReason implements ILeaveReason {

	private final String message;

	public KickLeaveReason(final String message) {
		if (message == null || message.isEmpty())
			this.message = "icked by the Server";
		else
			this.message = "icked by the Server (" + message + ")";
	}

	@Override
	public String getClientMessage() {
		return "You where k" + this.message;
	}

	@Override
	public String getLogMessage() {
		return "K" + this.message;
	}

	@Override
	public Side sideIssued() {
		return Side.SERVER;
	}

}