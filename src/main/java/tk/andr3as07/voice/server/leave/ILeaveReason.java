package tk.andr3as07.voice.server.leave;

import tk.andr3as07.voice.server.Side;

public interface ILeaveReason {

	public String getClientMessage();

	public String getLogMessage();

	public Side sideIssued();

}