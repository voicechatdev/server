package tk.andr3as07.voice.server.leave;

import tk.andr3as07.voice.server.Side;

public class TimeoutLeaveReason implements ILeaveReason {

	public TimeoutLeaveReason() {
	}

	@Override
	public String getClientMessage() {
		return "You Timed out";
	}

	@Override
	public String getLogMessage() {
		return "Timeout";
	}

	@Override
	public Side sideIssued() {
		return Side.CLIENT;
	}
}