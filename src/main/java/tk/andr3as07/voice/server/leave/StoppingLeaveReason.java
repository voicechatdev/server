package tk.andr3as07.voice.server.leave;

import tk.andr3as07.voice.server.Side;

public class StoppingLeaveReason implements ILeaveReason {

	public StoppingLeaveReason() {
	}

	@Override
	public String getClientMessage() {
		return "Server is Stopping";
	}

	@Override
	public String getLogMessage() {
		return "Server stopping";
	}

	@Override
	public Side sideIssued() {
		return Side.SERVER;
	}
}