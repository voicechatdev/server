package tk.andr3as07.voice.server.demonic;

public abstract class ClientIssuedJob {
	public abstract String completed(ClientIssuedJobResult result);

	public abstract int getJobId();

	public abstract ClientIssuedJobResult getResult();

	public abstract boolean isCompleted();

	public abstract boolean isResultSent();
}
