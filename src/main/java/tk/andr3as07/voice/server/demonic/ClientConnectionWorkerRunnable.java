package tk.andr3as07.voice.server.demonic;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import tk.andr3as07.voice.server.*;

public class ClientConnectionWorkerRunnable implements Runnable {

	Socket link;
	Scanner in;
	InputStream inStream;
	PrintWriter out;
	Server server;
	ArrayList<ClientIssuedJob> clientQueue;
	ArrayList<ServerIssuedJob> serverQueue;

	private final boolean running = true;

	Client client;

	public ClientConnectionWorkerRunnable(final Socket clientConnection, final Server server) throws IOException {
		this.link = clientConnection;
		this.inStream = this.link.getInputStream();
		this.in = new Scanner(this.inStream);
		this.out = new PrintWriter(this.link.getOutputStream());
		this.server = server;
		this.clientQueue = new ArrayList<ClientIssuedJob>();
		this.serverQueue = new ArrayList<ServerIssuedJob>();
	}

	private void close() {
		this.in.close();
		this.out.flush();
		this.out.close();
		try {
			this.link.close();
		} catch (final IOException e) {
			// TODO: Handle this [censored] Exception
		}

	}

	public boolean completeJob(final int jobID, final ClientIssuedJobResult result) {
		for (int i = 0; i < this.getQueueLength(); i++) {
			final ClientIssuedJob job = this.clientQueue.get(i);
			if (job.getJobId() == jobID) {
				job.completed(result);
				return true;
			}
		}
		return false;
	}

	public ClientIssuedJob getNextJob() {
		if (this.hasJobsInQueue())
			return this.clientQueue.get(0);
		else
			return null;
	}

	public int getQueueLength() {
		return this.clientQueue.size();
	}

	public boolean hasJobsInQueue() {
		return this.getQueueLength() > 0;
	}

	@Override
	public void run() {
		final String tmp = this.in.nextLine();
		long persistentID;
		try {
			persistentID = Long.parseLong(tmp);
		} catch (final NumberFormatException e) {
			this.out.write(DemonicErrorHandler.handle(JoinResponse.BAD_REQUEST));
			this.close();
			return;
		}

		this.client = new Client(persistentID, this.link.getInetAddress());

		final JoinResponse jresp = this.server.canJoin(this.client);

		if (jresp != JoinResponse.GRANTED) {
			this.out.write(DemonicErrorHandler.handle(jresp));
			this.close();
			return;
		}
		this.out.write(DemonicResponseHandler.handle(jresp));
		while (this.running) {
			if (!this.serverQueue.isEmpty())
				for (int i = 0; i < this.serverQueue.size(); i++) {
					final ServerIssuedJob job = this.serverQueue.get(i);
					if (job.currentTrys() == 0
							|| !job.isCompleted() && job.lastSentAt() + job.repeatAfter() > System.currentTimeMillis()
									&& job.currentTrys() < job.maxRetrys())
						this.out.write(JobWrapper.wrapServerIssuedJob(job));
				}
			if (this.hasJobsInQueue())
				for (int i = 0; i < this.getQueueLength(); i++) {
					final ClientIssuedJob job = this.clientQueue.get(i);
					if (job.isCompleted() && !job.isResultSent()) {
						this.out.write(DemonicResponseHandler.handle(job));
						this.clientQueue.remove(job);
					}
				}
			this.out.flush();
			String input = null;
			try {
				while (this.inStream.available() > 0)
					input += this.in.next();
			} catch (final IOException e) {
				// TODO Catch that [censored] Exception
			}
			if (input != null) {
				// TODO What shall we do with an unverified Input early in the
				// morning :)
			}
		}
	}

}
