package tk.andr3as07.voice.server.demonic;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class ServerIssuedJob implements Serializable {
	public abstract int currentTrys();

	public abstract long getId();

	public abstract short getType();

	public abstract boolean isCompleted();

	public abstract long lastSentAt();

	public abstract int maxRetrys();

	public abstract long repeatAfter();
}
