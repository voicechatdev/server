package tk.andr3as07.voice.server.group;

import java.util.*;

import tk.andr3as07.voice.server.Channel;
import tk.andr3as07.voice.server.Client;

public class GroupHandler {

	// ServergroupID, ChannelgroupID
	private int defaultServerGroup, defaultChannelGroup;

	// ID, Servergroup
	private final Map<Integer, ServerGroup> server;

	// ID, Channelgroup
	private final Map<Integer, ChannelGroup> channel;

	// Client, ServergroupIDs
	private final Map<Long, List<Integer>> clientServerGroupAssignment;

	// Client, <ChannelID, ChannelgroupID>
	private final Map<Long, Map<Long, Integer>> clientChannelGroupAssignment;

	public GroupHandler() {
		this.server = new HashMap<Integer, ServerGroup>();
		this.channel = new HashMap<Integer, ChannelGroup>();
		this.clientServerGroupAssignment = new HashMap<Long, List<Integer>>();
		this.clientChannelGroupAssignment = new HashMap<Long, Map<Long, Integer>>();
	}

	// public List<ServerGroup> getLadder(int ladder) {
	// List<ServerGroup> entrys = new ArrayList<ServerGroup>();
	// for(int id : server.keySet()) {
	// if(server.get(id).getLadder() == ladder) entrys.add(server.get(id));
	// }
	// return entrys;
	// }
	//
	// public ServerGroup getNext(ServerGroup group) {
	// List<ServerGroup> entrys = getLadder(group.getLadder());
	// byte smallest = (byte) 255;
	// ServerGroup best = null;
	// for(ServerGroup entry : entrys) {
	// if(entry.getRank() < smallest && entry.getRank() >= group.getRank()) {
	// smallest = entry.getRank();
	// best = entry;
	// }
	// }
	//// if(best == group) return null;
	// return best;
	// }

	public boolean addChannelgroup(final ChannelGroup group) {
		if (this.channel.containsKey(group.getID()) || this.channel.containsValue(group))
			return false;
		this.channel.put(group.getID(), group);
		return true;
	}

	public boolean addServergroup(final ServerGroup group) {
		if (this.server.containsKey(group.getID()) || this.server.containsValue(group))
			return false;
		this.server.put(group.getID(), group);
		return true;
	}

	public boolean addServergroupToClient(final Client client, final ServerGroup group) {
		List<Integer> groups;
		if (!this.clientServerGroupAssignment.containsKey(client.getPersistentID()))
			groups = new ArrayList<Integer>();
		else {
			groups = this.clientServerGroupAssignment.get(client.getPersistentID());
			if (groups.contains(group.getID()))
				return false;
		}
		groups.add(group.getID());
		this.clientServerGroupAssignment.put(client.getPersistentID(), groups);
		return true;
	}

	public boolean existsChannelgroup(final ChannelGroup group) {
		return this.channel.containsKey(group.getID());
	}

	public boolean existsServergroup(final ServerGroup group) {
		return this.server.containsKey(group.getID());
	}

	public ChannelGroup getChannelgroup(final int id) {
		if (!this.channel.containsKey(id))
			return null;
		return this.channel.get(id);
	}

	public ChannelGroup getClientChannelgroup(final Client client, final Channel channel) {
		if (!this.clientChannelGroupAssignment.containsKey(client.getPersistentID()))
			return this.getDefaultChannelgroup();

		final Map<Long, Integer> channelgroups = this.clientChannelGroupAssignment.get(client.getPersistentID());
		if (!channelgroups.containsKey(channel.getPersistentID()))
			return this.getDefaultChannelgroup();

		return this.getChannelgroup(channelgroups.get(channel.getPersistentID()));
	}

	public List<Integer> getClientServergroupIDs(final Client client) {
		if (!this.clientServerGroupAssignment.containsKey(client.getPersistentID())
				|| this.clientServerGroupAssignment.get(client.getPersistentID()).isEmpty()) {
			final List<Integer> assignments = new ArrayList<Integer>();
			assignments.add(this.defaultServerGroup);
			return assignments;
		}
		return this.clientServerGroupAssignment.get(client.getPersistentID());
	}

	public List<ServerGroup> getClientServergroups(final Client client) {
		final List<ServerGroup> groups = new ArrayList<ServerGroup>();
		for (final int id : this.getClientServergroupIDs(client))
			groups.add(this.getServergroup(id));
		return groups;
	}

	public ChannelGroup getDefaultChannelgroup() {
		return this.channel.get(this.defaultChannelGroup);
	}

	public ServerGroup getDefaultServergroup() {
		return this.server.get(this.defaultServerGroup);
	}

	public ServerGroup getServergroup(final int id) {
		if (!this.server.containsKey(id))
			return null;
		return this.server.get(id);
	}

	public boolean removeChannelgroup(final ChannelGroup group) {
		return this.channel.remove(group.getID(), group);
	}

	public boolean removeClientChannelgroup(final Client client, final Channel channel) {
		if (!this.clientChannelGroupAssignment.containsKey(client.getPersistentID()))
			return false;

		final Map<Long, Integer> channelgroups = this.clientChannelGroupAssignment.get(client.getPersistentID());
		if (!channelgroups.containsKey(channel.getPersistentID()))
			return false;

		channelgroups.remove(channel.getPersistentID());
		return true;
	}

	public boolean removeServergroup(final ServerGroup group) {
		return this.server.remove(group.getID(), group);
	}

	public boolean removeServergroupFromClient(final Client client, final ServerGroup group) {
		if (!this.clientServerGroupAssignment.containsKey(client.getPersistentID()))
			return false;
		final List<Integer> groups = this.clientServerGroupAssignment.get(client.getPersistentID());
		if (!groups.contains(group.getID()))
			return false;
		groups.remove(groups.indexOf(group.getID()));
		this.clientServerGroupAssignment.put(client.getPersistentID(), groups);
		return true;
	}

	public boolean setChannelgroupOfClient(final Client client, final Channel channel, final ChannelGroup group) {
		if (!this.existsChannelgroup(group))
			return false;
		Map<Long, Integer> channelgroups;
		if (!this.clientChannelGroupAssignment.containsKey(client.getPersistentID()))
			channelgroups = new HashMap<Long, Integer>();
		else
			channelgroups = this.clientChannelGroupAssignment.get(client.getPersistentID());
		final boolean changed = channelgroups.put(channel.getPersistentID(), group.getID()) != null;
		this.clientChannelGroupAssignment.put(client.getPersistentID(), channelgroups);
		return changed;
	}

	public boolean setDefaultChannelgroup(final ChannelGroup group) {
		if (!this.existsChannelgroup(group))
			return false;
		this.defaultChannelGroup = group.getID();
		return true;
	}

	public boolean setDefaultServergroup(final ServerGroup group) {
		if (!this.existsServergroup(group))
			return false;
		this.defaultServerGroup = group.getID();
		return true;
	}
}