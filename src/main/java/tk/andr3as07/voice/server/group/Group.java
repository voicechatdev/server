package tk.andr3as07.voice.server.group;

public class Group {

	private final String name;
	private final int id;

	protected Group(final String name, final int id) {
		this.name = name;
		this.id = id;
	}

	public final int getID() {
		return this.id;
	}

	public final String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return "{\"name\":\"" + this.name + "\",\"id\":" + this.id + "}";
	}
}